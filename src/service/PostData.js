export function PostData(type, userData) {

   // let BaseURL = 'https://api.thewallscript.com/restful/';
    //let BaseURL = 'http://localhost/PHP-Slim-Restful/api/';
    //let BaseURL = 'http://localhost/phpreactapi/api.php';
    let BaseURL = 'http://enterprise.staging-box.net/phpapi/api.php';
    console.log(BaseURL);
    let ApiURL = BaseURL+'?type='+type;
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(userData)
    };
    return new Promise((resolve, reject) =>{
    fetch(ApiURL, {
   method: 'POST',
   body: JSON.stringify(userData)
   })
   .then((response) => response.json())
   .then((res) => {
    console.log(res);
    resolve(res);
   })
   .catch((error) => {
    reject(error);
   });
   });

}