import React from 'react';
//import logo from './logo.svg';
import './App.css';
import './css/util.css';
import './css/main.css';
import './css/all.css';
import './css/adminlte.css';
import Login from './components/Login/Login';
import Products from './components/Products/Products';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

class App extends React.Component {

  constructor(props){
    super(props);
    this.state={
    appName: "React Project"
   }
  }

	render() {
		return (
          <Router>
            <Switch>
              <Route path="/" exact component ={Login}></Route>
              <Route path="/products" component ={Products}></Route>
              <Route path="/login"  component ={Login}></Route>
              
            </Switch>
          </Router>
         
		);
	}
} 


/* function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
} */

export default App;
