import React from 'react';
import {Redirect} from 'react-router-dom';

class Header extends React.Component {

    constructor(){
        super();
        this.state = {
        redirect: false
         };
         this.logout = this.logout.bind(this);
       }
       componentWillMount(){
           if(sessionStorage.getItem("userData")){
            //console.log('Call for feeds')
           }else{
            this.setState({redirect:true});
           }
       }
       logout(){
        sessionStorage.setItem("userData", '');
        sessionStorage.clear();
        this.setState({redirect:true});
       }

    render() {
        if (this.state.redirect){
            return (<Redirect to={'/login'}/>)
            }
        return (

        <nav className="main-header navbar navbar-expand navbar-white navbar-light">
            <ul className="navbar-nav">
            <li className="nav-item">
                <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars"></i></a>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
                <a href="/products" className="nav-link">Products</a>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
                <a href="#" className="nav-link" onClick={this.logout}>Logout</a>
            </li>
            </ul>
        </nav>

       );
       }
}

export default Header