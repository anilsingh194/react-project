import React from 'react';
import {Redirect} from 'react-router-dom';
import {PostData} from '../../service/PostData';



class Login extends React.Component {

    constructor(){
        super();
        this.state = {
       username: '',
       password: '',
       redirectToReferrer: false
       };
       this.login = this.login.bind(this);
       this.onChange = this.onChange.bind(this);
       }
       login() {
          
          if(this.state.username && this.state.password){
            PostData('login',this.state).then((result) => {
            let responseJson = result;
            //console.log(responseJson);
            if(responseJson.userData){
            sessionStorage.setItem('userData',JSON.stringify(responseJson));
            this.setState({redirectToReferrer: true});
            }
            });
            }  
            
       }
       
       
    onChange(e){
     
        this.setState({[e.target.name]:e.target.value});
    }
       
    render() {
      if (this.state.redirectToReferrer || sessionStorage.getItem('userData')){
        return (<Redirect to={'/products'}/>)
        }
        return (
          <div className="limiter">
           
    <div className="container-login100">
      <div className="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">

          <span className="login100-form-title p-b-33">
            Account Login (Try: anil.singh@hotmail.co.in / 123456)
          </span>

          <div className="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <input className="input100" type="text" name="username" placeholder="Username"  onChange={this.onChange} />
            <span className="focus-input100-1"></span>
            <span className="focus-input100-2"></span>
          </div>

          <div className="wrap-input100 rs1 validate-input" data-validate="Password is required">
            <input className="input100" type="password" name="password" placeholder="Password"  onChange={this.onChange} />
            <span className="focus-input100-1"></span>
            <span className="focus-input100-2"></span>
          </div>

          <div className="container-login100-form-btn m-t-20">
            <button className="login100-form-btn" onClick={this.login}>
              Sign in
            </button>
          </div>

          <div className="text-center p-t-45 p-b-4">
            <span className="txt1">
              Forgot
            </span>

            <a href="#" className="txt2 hov1">
              Username / Password?
            </a>
          </div>

          <div className="text-center">
            <span className="txt1">
              Create an account?
            </span>

            <a href="#" className="txt2 hov1">
              Sign up
            </a>
          </div>

      </div>
    </div>
  </div>
       );
       }
}

export default Login