import React from 'react';
import Header from '../Header/Header';
//import Footer from '../Footer/Footer';
import axios from 'axios';
import {Redirect} from 'react-router-dom';
class Products extends React.Component {

    constructor( props ) {
		super( props );

		this.state = {
			query: '',
			results: {},
			loading: false,
			message: '',
			totalResults: 0,
			totalPages: 0,
            currentPageNo: 0,
            sortby: '',
            redirectToReferrer: false
		};

        this.cancel = '';
      
    }
    componentWillMount() {
       if( sessionStorage.getItem('userData') == null){
        this.setState({redirectToReferrer: true});
       }
       
       const updatedPageNo = 0;
        const pageNumber = 0;
       const searchUrl = `http://localhost/phpapi/productapi.php`;
        console.log(searchUrl);
		if( this.cancel ) {
			this.cancel.cancel();
		}

		this.cancel = axios.CancelToken.source();

		 axios.get( searchUrl, {
			cancelToken: this.cancel.token
		} )
			.then( res => {
                console.log(res);
                const total = res.data.total;
            
				const totalPagesCount = this.getPageCount( total, 10 );
				const resultNotFoundMsg = ! res.data.hits.length
										? 'There are no more search results. Please try a new search'
										: '';
				this.setState( {
					results: res.data.hits,
					message: resultNotFoundMsg,
					totalResults: total,
					totalPages: totalPagesCount,
					currentPageNo: updatedPageNo,
                    loading: false,
                    sortby: this.state.sortby
				} )
			} )
			.catch( error => {
				if ( axios.isCancel(error) || error ) {
					this.setState({
						loading: false,
						message: 'Failed to fetch the data. Please check network'
					})
				}
			} ) 

    }
    fetchSearchResults = ( updatedPageNo = '', query, sort = '' ) => {
        const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
        //const sort       = sort ? `&sort=${sort}` : '';
        const sortby       = sort ? `&sortby=${sort}` : '';
        //const searchUrl = `https://pixabay.com/api/?key=17648474-47ad6500605c9d6eeb6134e4e&q=${query}${pageNumber}`;
       
        //const searchUrl = `http://irafinancialtrust.staging-box.net/myapi2.php?q=${query}${pageNumber}${sortby}`;
        const searchUrl = `http://localhost/phpapi/productapi.php?q=${query}${pageNumber}${sortby}`;
        console.log(searchUrl);
		if( this.cancel ) {
			this.cancel.cancel();
		}

		this.cancel = axios.CancelToken.source();

		axios.get( searchUrl, {
			cancelToken: this.cancel.token
		} )
			.then( res => {
                console.log(res);
                const total = res.data.total;
            
				const totalPagesCount = this.getPageCount( total, 10 );
				const resultNotFoundMsg = ! res.data.hits.length
										? 'There are no more search results. Please try a new search'
										: '';
				this.setState( {
					results: res.data.hits,
					message: resultNotFoundMsg,
					totalResults: total,
					totalPages: totalPagesCount,
					currentPageNo: updatedPageNo,
                    loading: false,
                    sortby: this.state.sortby
				} )
			} )
			.catch( error => {
                console.log(error);
				if ( axios.isCancel(error) || error ) {
					this.setState({
						loading: false,
						message: 'Failed to fetch the data. Please check network'
					})
				}
			} )
    };
    getPageCount = ( total, denominator ) => {
		const divisible	= 0 === total % denominator;
		const valueToBeAdded = divisible ? 0 : 1;
		return Math.floor( total/denominator ) + valueToBeAdded;
	};
    handleOnInputChange = ( event ) => {
        const query = event.target.value;
		const sortby = this.state.sortby;
		console.log(this.state.sortby);
		if ( ! query ) {
			this.setState( { query, results: {}, message: '', totalPages: 0, totalResults: 0, sortby:sortby } );
		} else {
			this.setState( { query, loading: true, message: '' }, () => {
				this.fetchSearchResults( 1, query, this.state.sortby );
			} );
		}
	};
    handleOnSelectChange = ( event ) => {
		const sortby = event.target.value;
		this.setState( {sortby:sortby});
        const query = this.state.query;
		 if ( ! sortby ) {
			this.setState( { query, results: {}, message: '', totalPages: 0, totalResults: 0,sortby:sortby } );
		} else {
			this.setState( { query, loading: true, message: '' }, () => {
				this.fetchSearchResults( 1, this.state.query,sortby );
			} );
		} 
    };
    
    renderSearchResults = () => {
        const { results } = this.state;
   
		if ( Object.keys( results ).length && results.length ) {
			return (
				<div className="results-container">
                    <table className="table">
          <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Detail</th>
            <th>Price</th>
            <th>Stock</th>
           
          </tr>
          </thead>
          <tbody>
        
         {/*  <ul>
      {elements.map((value, index) => {
        return <li key={index}>{value}</li>
      })}
    </ul> */}
         
					{ results.map( result => {
						return (
                            <tr>
                                 <th>{ result.id }</th>
                                <th>{result.name}</th>
                                <th>{result.detail}</th>
                                <th>{result.price}</th>
                                <th>{result.stock}</th>
							{/* <a key={ result.id } href={ result.previewURL } className="result-item">
								<h6 className="image-username">{result.user}</h6>
								<div className="image-wrapper">
									<img className="image" src={ result.previewURL } alt={`${result.username} image`}/>
								</div>
							</a> */}
                            </tr>
						)
					} ) }
 </tbody>
        </table>
				</div>
			)
		}
    };
    
    render() {
        console.log(this.state);
        if (this.state.redirectToReferrer){
            return (<Redirect to={'/login'}/>)
            }
            const { query, loading, message, currentPageNo, totalPages, sortby } = this.state;

		const showPrevLink = 1 < currentPageNo;
		const showNextLink = totalPages > currentPageNo;
		const mystyle = {
			'height': '90px',
			'margin-left': '10px',
			'margin-top': '-10px',
			'width': '50%'
		  };
        return (
            <React.Fragment>
            <Header />
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <a href="../../index3.html" className="brand-link">
                <span className="brand-text font-weight-light">AdminLTE 3</span>
                </a>
                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div className="info">
                        <a href="#" className="d-block">Rave Digital</a>
                        </div>
                    </div>
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li className="nav-item has-treeview">
                            <a href="#" className="nav-link">
                            <i className="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard
                                <i className="right fas fa-angle-left"></i>
                            </p>
                            </a>
                        </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <div className="content-wrapper">
                <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>Products Data</h1>
                        </div>

                    </div>
                </div>
                </section>
                <section className="content">
                   <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                <div className="card">
                                    <div className="card-header">
                                        {/* <h2 className="card-title">Responsive Hover Table</h2>
                                        <div className="card-tools">
                                            <div className="input-group input-group-sm">
                                                <input type="text" name="table_search" className="form-control float-right" placeholder="Search" />

                                                <div className="input-group-append">
                                                <button type="submit" className="btn btn-default"><i className="fas fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div> */}
                                    </div>

                                    <div className="card-body table-responsive p-0">
                                    <label className="search-label" htmlFor="search-input">
                                        <input
                                            type="text"
                                            name="query"
                                            value={ query }
                                            id="search-input"
                                            placeholder="Search..."
                                            onChange={this.handleOnInputChange}
                                            
                                        />
                                        <i className="fa fa-search search-icon" aria-hidden="true"/>
                                    </label>
                                            { this.renderSearchResults() }
                                    </div>

                                </div>
                            </div>
                       </div>
                    </div>
                </section>
            </div>
            </React.Fragment>
        );
    }
}

export default Products